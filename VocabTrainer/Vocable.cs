﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VocabTrainer
{
    public class Vocable
    {
        public string English { get; set; }
        public string Japanese { get; set; }

    }
}
