﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VocabTrainer
{
    public partial class DictPage : UserControl
    {
        public DictPage()
        {
            InitializeComponent();
        }

        private BindingList<Vocable> _bufferedList;

        public void ReLoadList()
        {
            _bufferedList = new BindingList<Vocable>(
                                    DataManager.Vocables.OrderBy(
                                        vocable => (radioButton1.Checked ? vocable.English : vocable.Japanese)).ToList());
            //if (dataGridView1.DataSource == null)
                dataGridView1.DataSource = _bufferedList;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            ReLoadList();
        }
    }
}
