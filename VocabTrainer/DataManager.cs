﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace VocabTrainer
{
    public class DataManager
    {
        private const string Path = "data.json";
        public static List<Vocable> Vocables => _vocables ?? (_vocables = ReadFromFile());

        private static List<Vocable> _vocables;

        private static List<Vocable> ReadFromFile()
        {
            var temp = File.Exists(Path) ? JsonConvert.DeserializeObject<List<Vocable>>(File.ReadAllText(Path)) : null;
            return temp ?? new List<Vocable>();
        }

        private static void WriteToFile()
        {
            File.WriteAllText(Path,JsonConvert.SerializeObject(Vocables));
        }

        public static void AddNewVocable(Vocable voc)
        {
            (_vocables ?? (_vocables = ReadFromFile())).Add(voc);
            WriteToFile();
        }

    }
}
